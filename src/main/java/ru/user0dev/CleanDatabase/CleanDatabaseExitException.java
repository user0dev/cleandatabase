package ru.user0dev.CleanDatabase;

public class CleanDatabaseExitException extends RuntimeException {
    public CleanDatabaseExitException(String text) {
        super(text);
    }
}
