package ru.user0dev.CleanDatabase;



import org.apache.commons.cli.*;


import java.io.Console;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class CleanDatabase {



    private static final String MY_CONFIG_NAME = ".my.cnf";

    private static class PHRASE {
        private static final String ASK_PASSWORD = "Enter password: ";
        private static final String DELETE_TEMPLATE = "Database '%s' contain %d tables. Are you sure you want to delete it: yes/no%n";
        private static final String DATABASE_EMPTY = "Database is empty";
    }

    private static final String USAGE = "cleandatabase.jar [OPTIONS] database";


    private static class ERROR_MESSAGE {
        private static final String WRONG_ARGUMENT_DATABASE = "You must specify one database";
        private static final String WRONG_PORT = "Wrong port value: ";
        private static final String WRONG_CONFIG_TEMPLATE = "Wrong config " + MY_CONFIG_NAME + "line: %d '%s'";
        private static final String SYSTEM_DATABASE = "You can not clean system databases";
    }

    private enum OPTION_NAMES {
        port, help, user, host, password
    }

    private static class DEFAULT {
        private static final int PORT = 3306;
        private static final String HOST = "localhost";
        private static final String USER = System.getProperty("user.name");
    }


    private static final Set<String> SYSTEM_DATABASES = Stream.of("systperformance_schema", "mysql", "information_schema", "sys").collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));


    private static final Options OPTIONS = new Options() {{
        addOption(Option.builder("h").longOpt("" + OPTION_NAMES.host).argName("address").hasArg().desc("name or address mysql server").build());
        addOption(Option.builder("p").longOpt("" + OPTION_NAMES.password).desc("Password to use when connecting to server.").hasArg().argName("password").build());
        addOption(Option.builder("u").longOpt("" + OPTION_NAMES.user).hasArg().desc("User for login if not current user.").argName("user").build());
        addOption(Option.builder("?").longOpt("" + OPTION_NAMES.help).desc("Display this help and exit.").build());
        addOption(Option.builder("P").longOpt("" + OPTION_NAMES.port).hasArg().desc("Port number to use for connection").argName("port").build());
    }};

    private static CommandLineParser cmdParser = new DefaultParser();
    private static HelpFormatter helpFormatter = new HelpFormatter();


    private static class Parameters {
        private String host = "";
        private int port = 0;
        private String user = "";
        private String password = "";
        private String database = "";

        @Override
        public String toString() {
            return "Parameters{" +
                    "host='" + host + '\'' +
                    ", port=" + port +
                    ", user='" + user + '\'' +
                    ", password='" + password + '\'' +
                    ", database='" + database + '\'' +
                    '}';
        }
    }

    /*
        Завершает работу с кодом 1 в случае ошибок.
     */
    private static Parameters optionProcessing(CommandLine cmd) {

        Parameters result = new Parameters();

        if (cmd.hasOption('?')) {
            helpFormatter.printHelp(USAGE, OPTIONS);
            System.exit(0);
        }

        String[] arguments = cmd.getArgs();
        if (arguments.length != 1 || arguments[0].trim().isEmpty()) {
            throw new CleanDatabaseExitException(ERROR_MESSAGE.WRONG_ARGUMENT_DATABASE);
        }
        result.database = arguments[0];

        if (cmd.hasOption("" + OPTION_NAMES.port)) {
            String portRawStr = cmd.getOptionValue(OPTION_NAMES.port.name());
            try {

                int portRaw = Integer.parseInt(portRawStr);
                if (portRaw < 1 || portRaw > 65535) {
                    throw new NumberFormatException();
                }
                result.port = portRaw;
            } catch (NumberFormatException ex) {
                throw new CleanDatabaseExitException(ERROR_MESSAGE.WRONG_PORT + portRawStr);
            }
        }

        String rawUser = cmd.getOptionValue("" + OPTION_NAMES.user);
        if (rawUser != null && !rawUser.trim().isEmpty()) {
            result.user = rawUser;
        }

        String rawPassword = cmd.getOptionValue("" + OPTION_NAMES.password);
        if (rawPassword != null && !rawPassword.isEmpty()) {
            result.password = rawPassword;
        }

        String rawHost = cmd.getOptionValue("" + OPTION_NAMES.host);
        if (rawHost != null && !rawHost.trim().isEmpty()) {
            result.host = rawHost;
        }

        return result;

    }
    
    private static String getMyConfigName() {
        return System.getProperty("user.home") + "/" + MY_CONFIG_NAME;
    }

    //распарсю так. нафиг. Можно было через Apache Commons Configuration или ini4j
    /*
    Завершает работу с кодом 1 в случае ошибок при разборе конфига. Заполнит только те параметры которые есть в конфиге
    Если файла нет вернет false
     */
    private static Parameters loadMyConf() throws IOException {
        Parameters result = new Parameters();
        try {

            String configFullName = getMyConfigName();
//            System.out.println(configFullName);
            String[] config = Files.lines(Paths.get(configFullName)).map(String::trim).filter(s -> !s.isEmpty()).toArray(String[]::new);
            boolean clientSection = false;
            for (int i = 0; i < config.length; i++) {
                String line = config[i];
                if (clientSection && line.startsWith("[")) {
                    break;
                }
                if (line.equalsIgnoreCase("[client]")) {
                    clientSection = true;
                    continue;
                }
                if (clientSection) {
                    String[] splitLine = line.split("=");
                    if (splitLine.length != 2) {
                        throw new CleanDatabaseExitException(String.format(ERROR_MESSAGE.WRONG_CONFIG_TEMPLATE, i + 1, line));
                    }
                    String key = splitLine[0].trim();
                    String value = splitLine[1].trim();
                    if (key.isEmpty()) {
                        throw new CleanDatabaseExitException(String.format(ERROR_MESSAGE.WRONG_CONFIG_TEMPLATE, i + 1, line));

                    }
                    value = value.replaceAll("^\"|\"$", "");
                    if (value.isEmpty()) {
                        continue;
                    }

                    switch (key) {
                        case "password":
                            result.password = value;
                            break;
                        case "user":
                            result.user = value;
                            break;
                        case "host":
                            result.host = value;
                            break;
                        case "port":
                            try {
                                int rawPort = Integer.parseInt(value);
                                if (rawPort < 1 || rawPort > 65536) {
                                    throw new NumberFormatException();
                                }
                                result.port = rawPort;
                            } catch (NumberFormatException ex) {
                                throw new CleanDatabaseExitException(String.format(ERROR_MESSAGE.WRONG_CONFIG_TEMPLATE, i + 1, line));
                            }
                            break;
                    }
                }
            }
        } catch (java.nio.file.NoSuchFileException ex) {
            return result;
        }
        return result;
    }

    //если не сможет получить терминал, спросит через scanner
    private static String askPassword() {
        System.out.print(PHRASE.ASK_PASSWORD);
        Console terminal = System.console();
        if (terminal != null) {
            char[] rawPassword = terminal.readPassword();
            return new String(rawPassword);
        } else {
            Scanner sc = new Scanner(System.in);
            return sc.nextLine();
        }
    }


    private static Connection databaseInit(String host, int port, String user, String password, String database) throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        //} catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
        } catch (Exception ex) {
            System.err.println(ex);
        }
        String sqlUrl = String.format("jdbc:mysql://%s:%d/%s?user=%s&password=%s&useSSL=false", host, port, database, user, password);
        try {
            return DriverManager.getConnection(sqlUrl);
        } catch (SQLException ex) {
            if (ex.getErrorCode() == 1049) {
                throw new CleanDatabaseExitException(ex.getMessage());
            }
            throw ex;
        }
    }

    public static void main(String[] args) throws ParseException, IOException, SQLException {

        try {
            CommandLine cmd = cmdParser.parse(OPTIONS, args);

            Parameters parOpt = optionProcessing(cmd);
            String database = parOpt.database;

//            System.out.println("parOpt: " + parOpt);

            if (SYSTEM_DATABASES.contains(parOpt.database)) {
                throw new CleanDatabaseExitException(ERROR_MESSAGE.SYSTEM_DATABASE);

            }
            Parameters parConf;
            if (parOpt.port == 0 || parOpt.user.isEmpty() || parOpt.host.isEmpty() || parOpt.password.isEmpty()) {
                parConf = loadMyConf();
            } else {
                parConf = new Parameters();
            }

//            System.out.println("parConf: " + parConf);

            String password;



            if (!parOpt.password.isEmpty()) {
                password = parOpt.password;
            } else if (!parConf.password.isEmpty()) {
                password = parConf.password;
            } else {
                password = askPassword();
            }


            int port = parOpt.port != 0 ? parOpt.port : parConf.port != 0 ? parConf.port : DEFAULT.PORT;

            String host = !parOpt.host.isEmpty() ? parOpt.host : !parConf.host.isEmpty() ? parConf.host : DEFAULT.HOST;

            String user = !parOpt.user.isEmpty() ? parOpt.user : !parConf.user.isEmpty() ? parConf.user : DEFAULT.USER;

//            System.out.printf("host: %s%npassword: %s%nuser: %s%nport: %d%ndatabase: %s%n", host, password, user, port, database);


            Connection connection = databaseInit(host, port, user, password, database);
            try (
                    PreparedStatement stmtShow = connection.prepareStatement("SHOW TABLES");
                    Statement stmtDrop = connection.createStatement();
            ) {
                List<String> dbNames = new ArrayList<>();
                try (ResultSet rs = stmtShow.executeQuery()) {
                    while (rs.next()) {
                        dbNames.add(rs.getString(1));
                    }
                }
                if (dbNames.size() == 0) {
                    System.out.println(PHRASE.DATABASE_EMPTY);
                } else {
                    System.out.printf(PHRASE.DELETE_TEMPLATE, database, dbNames.size());
                    Scanner sc = new Scanner(System.in);
                    if (sc.next().trim().equals("yes")) {
                        for (String dbName : dbNames) {
                            stmtDrop.execute("DROP TABLE IF EXISTS " + dbName);
                        }
                    }
                }
            }

        } catch (CleanDatabaseExitException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        } catch (SQLException ex) {
            System.err.printf("%s%nError code: %d%nSQL State: %s%n", ex.getMessage(), ex.getErrorCode(), ex.getSQLState());
            System.exit(1);
        }

    }
}
